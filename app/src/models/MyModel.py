class MyModel:
    """
    Class defining the model.
    """

    def __init__(self):
        print("__init__()")

    
    def load_weights(self, path_to_weights: str) -> None:
        """
        Function that loads the weights into the model.

        Args:
            path_to_weights: path to the .h5 file
        """
    
        print("load_weights()")