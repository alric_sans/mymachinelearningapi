# MyMachineLearningAPI

Use the following commands in order to use the api :

```powershell
git clone https://gitlab.com/alric_sans/mymachinelearningapi.git
```

```powershell
cd mymachinelearningapi
```

```powershell
docker build -t myapi .
```

```powershell
docker run -d -p 8000:8000 myapi
```

Little change.

Then you should be able to visit :
[localhost:8000/docs](localhost:8000/docs) and see FastAPI web interface.
