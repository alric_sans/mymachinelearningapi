FROM python:3.9.5-buster

COPY ./app /app
COPY ./requirements.txt /req/requirements.txt

RUN apt-get update
RUN apt-get -y install libc-dev
RUN apt-get -y install build-essential
RUN pip3 install --upgrade pip && pip3 install -r /req/requirements.txt

EXPOSE 8000

CMD ["uvicorn", "app.src.main:app", "--host", "0.0.0.0", "--port", "8000"]